exports.handler = function (context, event, callback) {

    const moment = require('moment-timezone');

    //Inicio do cadastramento de feriados

    var data = [{
        "dia": [1, 2],
        "mes": 0 //Janeiro
    }, {
        "dia": [],//Alterado por Edu (Removido dia 16-02)
        "mes": 1 //Fevereiro
    }, {
        "dia": [],//Alterado por Edu (Removido dia 9 no dia 08-03)
        "mes": 2 //Março
    }, {
        "dia": [2, 21], //Alterado por Rafa (adicionado dia 2 no dia 30-03)
        "mes": 3 //Abril
    }, {
        "dia": [1],
        "mes": 4 //Maio
    }, {
        "dia": [3],
        "mes": 5 //Junho
    }, {
        "dia": [],
        "mes": 6 //Julho
    }, {
        "dia": [],
        "mes": 7 //Agosto
    }, {
        "dia": [7],
        "mes": 8 //Setembro
    }, {
        "dia": [12],
        "mes": 9 //Outubro
    }, {
        "dia": [2, 15],
        "mes": 10 //Novembro
    }, {
        "dia": [24, 25],
        "mes": 11 //Dezembro
    }];

    //fim cadastramento Feridados

    function getHolidayByMonth(code) {
        return data.filter(
            function (data) {
                return data.mes == code
            }
        );
    }

    timee = new Date();
    const zonedTime = moment(timee.getTime()).tz("America/Sao_Paulo");
    console.log(timee);
    console.log(zonedTime);
    const diaCalendario = zonedTime.date();
    const month = zonedTime.month();
    const diaDaSemana = zonedTime.weekday();
    const hora = zonedTime.hour();
    const minutos = zonedTime.minute();

    console.log("DIA:", diaCalendario, "\nMÊS: ", month, "\nDIA DA SEMANA: ",
        diaDaSemana, "\nHORA: ", hora, "\nMINUTOS: ", minutos);

    const holiday = getHolidayByMonth(month);
    console.log('HOLIDAY : ' + holiday[0].dia.length);
    if (holiday.length > 0) {
        const holidayStatus = holiday[0].dia.indexOf(diaCalendario);
        if (holidayStatus >= 0) {
            console.log('HOJE EH FERIADO');
            callback(null, { horario: false, dia: diaCalendario, hora: `${hora}:${minutos}` });
        }
    }
    console.log('NAO EH FERIADO');
    console.log(`DIA:${diaDaSemana}; HORA:${hora}`);
    if ((diaDaSemana >= 1 && diaDaSemana <= 5 && hora >= 7 && hora < 19)
        || (diaDaSemana === 6 && hora >= 7 && hora < 13)) {
        callback(null, { horario: true, dia: diaCalendario, hora: `${hora}:${minutos}` });
    }
    callback(null, { horario: false, dia: diaCalendario, hora: `${hora}:${minutos}` });
};
// 0 = domingo, 1 = segunda, 6 = sábado