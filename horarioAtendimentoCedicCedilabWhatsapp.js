exports.handler = function (context, event, callback) {
    
    const moment = require("moment-timezone");
    
    const mock_business_hour = context.MOCK_BUSINESS_HOUR;
    const mock_hour = event.mock_hour;
    const isValidMockHour = mock_hour && moment(mock_hour, 'YYYY-MM-DD HH:mm:ss',true).isValid();

    const datasFeriados = [
      { "mes": 0, "dia": [1] }, //Janeiro
      { "mes": 1, "dia": [28] }, //Fevereiro
      { "mes": 2, "dia": [1, 2, 23] }, //Março
      { "mes": 3, "dia": [15, 21] }, //Abril
      { "mes": 4, "dia": [1] }, //Maio
      { "mes": 5, "dia": [16] }, //Junho
      { "mes": 6, "dia": [] }, //Julho
      { "mes": 7, "dia": [] }, //Agosto
      { "mes": 8, "dia": [7] }, //Setembro
      { "mes": 9, "dia": [12, 15, 28] }, //Outubro
      { "mes": 10, "dia": [2, 15] }, //Novembro
      { "mes": 11, "dia": [8, 25] }, //Dezembro
    ];
    
    const time = mock_business_hour && isValidMockHour ?  new Date(mock_hour) : new Date();
    const zonedTime = isValidMockHour ? moment(time.getTime()) :moment(time.getTime()).tz("America/Sao_Paulo");
    console.log(zonedTime);
    const diaCalendario = zonedTime.date();
    const month = zonedTime.month();
    const diaDaSemana = zonedTime.weekday();
    const hora = zonedTime.hour();
    const minutos = zonedTime.minute();
    const segundaASexta = (diaDaSemana >= 1 && diaDaSemana <= 5);
    const feriados = datasFeriados.filter (hlds => hlds.mes == month);
    const ehFeriado = feriados.length > 0 && feriados[0].dia.includes(diaCalendario);
    let dentroDoHorario = false
    
    if (diaDaSemana === 0){
        dentroDoHorario = false;
    }else{
        if (segundaASexta && !ehFeriado && (hora >= 8 && hora < 20)){
            dentroDoHorario = true; 
        }
			else {
				if ((segundaASexta && !ehFeriado) && hora === 7 ){
					if(minutos >= 30){
                            dentroDoHorario = true;
                        }else{
                            dentroDoHorario = false;
                        }
				}else{
					if (diaDaSemana === 6 || ehFeriado){
						if (hora >= 7 && hora < 12){
							dentroDoHorario = true;
						}else{
							 if (hora === 13 ){
								if(minutos <= 20){
									dentroDoHorario = true;
								}else{
									dentroDoHorario = false;
								}
							 }else{
								dentroDoHorario = false;
							 }
						} 
					}else{
						dentroDoHorario = false;
					}
				}
			}
		}
	
    
    console.log(`
    | DATA: ${diaCalendario}/${month + 1} ${hora}:${minutos}
    | DIA DA SEMANA: ${diaDaSemana}
    | FERIADO: ${ehFeriado}
    | SEGASEX: ${segundaASexta}
    | NOHORARIO: ${dentroDoHorario}
    | MOCK: ${mock_business_hour}
    | MOCK_HOUR: ${mock_hour}
    `);
    
    const objRetorno = {
        horario: dentroDoHorario,
        data: `${diaCalendario}/${month + 1} ${hora}:${minutos}`,
        dia_semana: diaDaSemana,
        feriado: ehFeriado,
        segunda_a_sexta: segundaASexta,
        mock: mock_business_hour,
        valor_mock:mock_hour
    };
    
    return callback(null, objRetorno);
    
};