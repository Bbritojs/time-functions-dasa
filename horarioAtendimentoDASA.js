exports.handler = function (context, event, callback) {
    const moment = require("moment-timezone");

    // Inicio do cadastramento de feriados

    const data = [
        { "mes": 0, "dia": [] }, //Janeiro
        { "mes": 1, "dia": [] }, //Fevereiro
        { "mes": 2, "dia": [] }, //Março
        { "mes": 3, "dia": [21] }, //Abril
        { "mes": 4, "dia": [1] }, //Maio
        { "mes": 5, "dia": [3] }, //Junho
        { "mes": 6, "dia": [9] }, //Julho
        { "mes": 7, "dia": [] }, //Agosto
        { "mes": 8, "dia": [7] }, //Setembro
        { "mes": 9, "dia": [12] }, //Outubro
        { "mes": 10, "dia": [2, 15, 20] }, //Novembro
        { "mes": 11, "dia": [25] }, //Dezembro
    ];

    // fim cadastramento Feridados

    function getHolidayByMonth(code) {
        return data.filter(function (data) {
            return data.mes == code;
        });
    }

    const timee = new Date();
    const zonedTime = moment(timee.getTime()).tz("America/Sao_Paulo");
    console.log(timee);
    console.log(zonedTime);
    const diaCalendario = zonedTime.date();
    const month = zonedTime.month();
    const diaDaSemana = zonedTime.weekday();
    const hora = zonedTime.hour();
    const minutos = zonedTime.minute();
    let isFeriado = false;

    console.log("DIA:", diaCalendario, "\nMÊS: ", month, "\nDIA DA SEMANA: ",
        diaDaSemana, "\nHORA: ", hora, "\nMINUTOS: ", minutos);

    const holiday = getHolidayByMonth(month);
    console.log(`HOLIDAY : ${holiday[0].dia.length}`);
    if (holiday.length > 0) {
        const holidayStatus = holiday[0].dia.indexOf(diaCalendario);
        if (holidayStatus >= 0) {
            isFeriado = true;
        }
    }
    if ((!isFeriado && diaDaSemana >= 1 && diaDaSemana <= 5 && hora >= 6 && hora < 22)
        || ((diaDaSemana === 6 || isFeriado) && hora >= 6 && hora < 18)) {
        callback(null, { horario: true, dia: diaCalendario, hora: `${hora}:${minutos}` });
    }
    callback(null, { horario: false, dia: diaCalendario, hora: `${hora}:${minutos}` });
};
  // 0 = domingo, 1 = segunda, 6 = sábado
