exports.handler = function (context, event, callback) {

    const moment = require("moment-timezone");

    // Inicio do cadastramento de feriados
    const data = [
        {
            dia: [1],
            mes: 0, // Janeiro
        },
        {
            dia: [],
            mes: 1, // Fevereiro
        },
        {
            dia: [1],
            mes: 2, // Março
        },
        {
            dia: [15, 21],
            mes: 3, // Abril
        },
        {
            dia: [1],
            mes: 4, // Maio
        },
        {
            dia: [16],
            mes: 5, // Junho
        },
        {
            dia: [],
            mes: 6, // Julho
        },
        {
            dia: [],
            mes: 7, // Agosto
        },
        {
            dia: [7],
            mes: 8, // Setembro
        },
        {
            dia: [12],
            mes: 9, // Outubro
        },
        {
            dia: [2, 15],
            mes: 10, // Novembro
        },
        {
            dia: [25],
            mes: 11, // Dezembro
        },
    ];

    // fim cadastramento Feridados

    function getHolidayByMonth(code) {
        return data.filter(function (data) {
            return data.mes === code;
        });
    }

    const timee = new Date();
    const zonedTime = moment(timee.getTime()).tz("America/Sao_Paulo");
    console.log(timee);
    console.log(zonedTime);
    const diaCalendario = zonedTime.date();
    const month = zonedTime.month();
    const diaDaSemana = zonedTime.weekday();
    const hora = zonedTime.hour();
    const minutos = zonedTime.minute();
    let isFeriado = false;

    console.log("DIA:", diaCalendario, "\nMÊS: ", month, "\nDIA DA SEMANA: ",
        diaDaSemana, "\nHORA: ", hora, "\nMINUTOS: ", minutos);

    const holiday = getHolidayByMonth(month);
    console.log(`HOLIDAY : ${holiday[0].dia.length}`);
    if (holiday.length > 0) {
        const holidayStatus = holiday[0].dia.indexOf(diaCalendario);
        if (holidayStatus >= 0) {
            isFeriado = true;
        }
    }

    let uraHabilitada = false;

    if (isFeriado && diaDaSemana !== 0) {
        if (hora >= 7 && (hora < 13 || (hora === 13 && minutos < 20))) {
            uraHabilitada = true;
        }
    } else {
        if ((diaDaSemana >= 1 && diaDaSemana <= 5 && (hora > 6 || (hora === 6 && minutos >= 30)) && hora < 19)
            || (diaDaSemana === 6 && hora >= 7 && (hora < 13 || (hora === 13 && minutos < 20)))) {
            uraHabilitada = true;
        }
    }

    callback(null, { horario: uraHabilitada, dia: diaCalendario, hora: `${hora}:${minutos}` });
};
// 0 = domingo, 1 = segunda, 6 = sábado
