exports.handler = function (context, event, callback) {
    const moment = require("moment-timezone");
    
    const mock_business_hour = context.MOCK_BUSINESS_HOUR;
    const mock_hour = event.mock_hour;
    const isValidMockHour = mock_hour && moment(mock_hour, 'YYYY-MM-DD hh:mm:ss',true).isValid();

    const datasFeriados = [
        { "mes": 0, "dia": [1, 20] }, //Janeiro
        { "mes": 1, "dia": [] }, //Fevereiro
        { "mes": 2, "dia": [1] }, //Março
        { "mes": 3, "dia": [15, 17, 21, 23] }, //Abril
        { "mes": 4, "dia": [1] }, //Maio
        { "mes": 5, "dia": [16] }, //Junho
        { "mes": 6, "dia": [9] }, //Julho
        { "mes": 7, "dia": [] }, //Agosto
        { "mes": 8, "dia": [7] }, //Setembro
        { "mes": 9, "dia": [12] }, //Outubro
        { "mes": 10, "dia": [2, 15, 20] }, //Novembro
        { "mes": 11, "dia": [25] }, //Dezembro
    ];

    const time = mock_business_hour && isValidMockHour ?  new Date(mock_hour) : new Date();
    const zonedTime = moment(time.getTime()).tz("America/Sao_Paulo");
    console.log(zonedTime);
    const diaCalendario = zonedTime.date();
    const month = zonedTime.month();
    const diaDaSemana = zonedTime.weekday();
    const hora = zonedTime.hour();
    const minutos = zonedTime.minute();
    const segundaASexta = (diaDaSemana >= 1 && diaDaSemana <= 5);
    const feriados = datasFeriados.filter (hlds => hlds.mes == month);
    const ehFeriado = feriados.length > 0 && feriados[0].dia.includes(diaCalendario);
    let dentroDoHorario = false
    
    if (segundaASexta && (hora >= 6 && hora < 21)){
        dentroDoHorario = true;
    }else{
        if((diaDaSemana === 6 || ehFeriado) && (hora >= 6 && hora < 17)){
            dentroDoHorario = true;
        }
    }  
    
    console.log(`
    | DATA: ${diaCalendario}/${month + 1} ${hora}:${minutos}
    | DIA DA SEMANA: ${diaDaSemana}
    | FERIADO: ${ehFeriado}
    | SEGASEX: ${segundaASexta}
    | NOHORARIO: ${dentroDoHorario}
    | MOCK: ${mock_business_hour}
    | MOCK_HOUR: ${mock_hour}
    `);
    
    const objRetorno = {
        horario: dentroDoHorario,
        data: `${diaCalendario}/${month + 1} ${hora}:${minutos}`,
        dia_semana: diaDaSemana,
        feriado: ehFeriado,
        segunda_a_sexta: segundaASexta,
        mock: mock_business_hour,
        valor_mock:mock_hour
    };
    
    return callback(null, objRetorno);
};
  // 0 = domingo, 1 = segunda, 6 = sábado
